## Unsupervised Learning
### Project 3: Creating Customer Segments

Analyze a dataset containing data on various customers' annual spending amounts.

[UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets/Wholesale+customers)
